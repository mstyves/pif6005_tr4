from LoadData import *

# Global resultats
# Hyppothesis: keeping most similar result between predictions of identical data will increase accuracy
# Considération seulement du 10 fold cross validation

result=[]

from NeuralNetwork import *
result.append(pred_10fcv)
#result.append(pred_cv)

from DecisionTree import *
result.append(pred_10fcv)
#result.append(pred_cv)

from NaiveBayes import *
result.append(pred_10fcv)
#result.append(pred_cv)

from RandomForest import *
result.append(pred_10fcv)
#result.append(pred_cv)

from SupportVectorMachine import *
result.append(pred_10fcv)
#result.append(pred_cv)

print("----Tableau comparatif du système #1----")
print_comparative(formated_data["sys1"]["resultats"])
print("----Tableau comparatif du système #2----")
print_comparative(formated_data["sys2"]["resultats"])

print("------------------------Validation de la prédiction la plus significative------------------------")
# if sum of prediction is > then half of number of prediction keep 1 else keep 0
result_somme=[]
nb_resultat=len(result)
nb_predictions=len(pred_10fcv)

for i in range(nb_predictions):
    sommes=0
    for j in range(nb_resultat):
        #print(result[j][i])
        sommes += result[j][i]

    if(sommes > (nb_resultat/2)):
        result_somme.append(1)
    else:
        result_somme.append(0)

cm = confusion_matrix(formated_data["sys1"]["target"], result_somme)
print("Matrice de confusion(apprentissage combiné du système #2 et test sur le système #1")
print_cm(cm, ["1","0"])

print("Précision de la prédiction combinée des données du système #1: %.4f" % accuracy_score(formated_data["sys1"]["target"],result_somme))
