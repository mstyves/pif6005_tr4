from pandas import *
from sklearn.model_selection import train_test_split
# Lib used in all sub py files
from pprint import pprint
from sklearn.metrics import  confusion_matrix
from sklearn.metrics import accuracy_score

# Construction of a reusable structure for all learning algorithms
formated_data = {}
formated_data["sys1"] = {}
formated_data["sys1"]["resultats"]=[]
formated_data["sys2"] = {}
formated_data["sys2"]["resultats"]=[]

# load xlsx file and change blank or NA data to zero
formated_data["sys1"]["df"] = read_excel(io="Donnees/Systeme-1.xlsx", sheet_name="Classification Binaire").replace(r'\s+', 0, regex=True).fillna(value=0)
formated_data["sys1"]["target"] = formated_data["sys1"]["df"]['HT Effort']
formated_data["sys1"]["Ressources"] = formated_data["sys1"]["df"]['Ressources']
formated_data["sys1"]["data"] = formated_data["sys1"]["df"].drop(['HT Effort', 'Ressources'], axis=1)
formated_data["sys1"]["X_train"], formated_data["sys1"]["X_test"], formated_data["sys1"]["y_train"], formated_data["sys1"]["y_test"] = train_test_split(formated_data["sys1"]["data"], formated_data["sys1"]["target"], test_size=0.15)

formated_data["sys2"]["df"] = read_excel(io="Donnees/Systeme-2.xlsx", sheet_name="Classification Binaire").replace(r'\s+', 0, regex=True).fillna(value=0)
formated_data["sys2"]["target"] = formated_data["sys2"]["df"]['HT Effort']
formated_data["sys2"]["Ressources"] = formated_data["sys2"]["df"]['Ressources']
formated_data["sys2"]["data"] = formated_data["sys2"]["df"].drop(['HT Effort', 'Ressources'], axis=1)
formated_data["sys2"]["X_train"], formated_data["sys2"]["X_test"], formated_data["sys2"]["y_train"], formated_data["sys2"]["y_test"] = train_test_split(formated_data["sys2"]["data"], formated_data["sys2"]["target"], test_size=0.15)

def print_cm(cm, labels, hide_zeroes=False, hide_diagonal=False, hide_threshold=None):
    """pretty print for confusion matrixes"""
    columnwidth = max([len(x) for x in labels] + [5])  # 5 is value length
    empty_cell = " " * columnwidth
    # Print header
    print("    " + empty_cell, end=" ")
    for label in labels:
        print("%{0}s".format(columnwidth) % label, end=" ")
    print()
    # Print rows
    for i, label1 in enumerate(labels):
        print("    %{0}s".format(columnwidth) % label1, end=" ")
        for j in range(len(labels)):
            cell = "%{0}.0f".format(columnwidth) % cm[i, j]
            if hide_zeroes:
                cell = cell if float(cm[i, j]) != 0 else empty_cell
            if hide_diagonal:
                cell = cell if i != j else empty_cell
            if hide_threshold:
                cell = cell if cm[i, j] > hide_threshold else empty_cell
            print(cell, end=" ")
        print()

def print_comparative(results):
    """pretty print for confusion matrixes"""
    labels = list(map(lambda x: x['name'], results))
    #print(results)
    columnwidth = max([len(x) for x in labels] + [5])  # 5 is value length
    empty_cell = " " * columnwidth
    # Print header
    print("           " + empty_cell, end=" ")
    for label in labels:
        print("%{0}s".format(columnwidth) % label, end=" ")
    print()
    # Print rows
    print("Appr 10fcv " + empty_cell, end=" ")
    for j in range(len(labels)):
        cell = "%{0}.4f".format(columnwidth) % results[j]['score_10fcv']
        print(cell, end=" ")
    print()
    print("Appr cv    " + empty_cell, end=" ")
    for j in range(len(labels)):
        cell = "%{0}.4f".format(columnwidth) % results[j]['score_cv']
        print(cell, end=" ")
    print()

    # Print rows
    print("Tests 10fcv" + empty_cell, end=" ")
    for j in range(len(labels)):
        cell = "%{0}.4f".format(columnwidth) % results[j]['accuracy_10fcv']
        print(cell, end=" ")
    print()
    print("Tests cv   " + empty_cell, end=" ")
    for j in range(len(labels)):
        cell = "%{0}.4f".format(columnwidth) % results[j]['accuracy_cv']
        print(cell, end=" ")
    print()

def print_result(score_10fcv, score_cv, accuracy_10fcv, accuracy_cv, cm_10fcv, cm_cv):
    begin_space="      "
    print("Précision de l'apprentissage")
    print(begin_space,"10 fold cross validation: %.4f" % score_10fcv)
    print(begin_space,"Validation croisée: %.4f" % score_cv)
    print("Précision sur un autre système")
    print(begin_space,"10 fold cross validation: %.4f" % accuracy_10fcv)
    print(begin_space,"Validation croisée: %.4f" % accuracy_cv)
    print("Matrices de confusion")
    print(begin_space,"10 fold cross validation")
    print_cm(cm_10fcv, ["1","0"])
    print(begin_space,"Cross validation")
    print_cm(cm_cv, ["1","0"])