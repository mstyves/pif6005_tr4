#PIF6005 Travail #4

Appliquer les techniques suivantes sur des données de métriques logicielles:

- Artificial Neural Network
- Decision Tree
- Naive Bayes Classifier
- Random Forest
- Support Vector Machines


#Configuration du projet

#Requis
##Interpreteur Python3
Sur OsX

brew install python3.6


## Lire des fichiers de données
pip install pandas

##Installation de scikit
pip install sscikit-learn

#Utilisation
Pour executer tous les algorithmes d'apprentissages

python AllML.py

# Références de librairies
- [10 fold cross validation](http://scikit-learn.org/stable/modules/cross_validation.html)
- [Artificial Neural Network](http://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html)
- [Decision tree](http://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html)
- [Naive Bayes Classifier](http://scikit-learn.org/stable/modules/naive_bayes.html)
- [Random forest](http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html)
- [Support Vector Machine](http://scikit-learn.org/stable/modules/svm.html)
