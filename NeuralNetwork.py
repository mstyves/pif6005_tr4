from LoadData import *
from sklearn.neural_network import MLPClassifier

print("------------------------Neural Network------------------------")
print("------------------------Système #1------------------------")
# Network definition (11 inputs values, up to 1000 hidden leaf and binary output )
ml_10fcv = MLPClassifier(hidden_layer_sizes=(11,1000,2),solver='lbfgs',learning_rate_init=0.1,max_iter=50000, tol=0.1)
ml_cv = MLPClassifier(hidden_layer_sizes=(11,1000,2),solver='lbfgs',learning_rate_init=0.1,max_iter=50000, tol=0.1)

ml_10fcv.fit(formated_data["sys1"]["X_train"], formated_data["sys1"]["y_train"])
score_10fcv = ml_10fcv.score(formated_data["sys1"]["X_test"],formated_data["sys1"]["y_test"])
ml_cv.fit(formated_data["sys1"]["data"], formated_data["sys1"]["target"])
score_cv = ml_cv.score(formated_data["sys2"]["data"],formated_data["sys2"]["target"])

# Second system prediction
pred_10fcv = ml_10fcv.predict(formated_data["sys2"]["data"])
accuracy_10fcv = accuracy_score(formated_data["sys2"]["target"],pred_10fcv)
pred_cv = ml_cv.predict(formated_data["sys2"]["data"])
accuracy_cv = accuracy_score(formated_data["sys2"]["target"],pred_cv)

# Stockage pour le formattage des données comparatives
formated_data["sys1"]["resultats"].append({'name':"Neural Network",'score_10fcv':score_10fcv,'score_cv':score_cv, 'accuracy_10fcv':accuracy_10fcv,'accuracy_cv':accuracy_cv})

cm_10fcv = confusion_matrix(formated_data["sys2"]["target"], pred_10fcv)
cm_cv = confusion_matrix(formated_data["sys2"]["target"], pred_cv)

print_result(score_10fcv=score_10fcv, score_cv=score_cv, accuracy_10fcv=accuracy_10fcv, accuracy_cv=accuracy_cv, cm_10fcv=cm_10fcv, cm_cv=cm_cv)

print("------------------------Système #2------------------------")
ml_10fcv = MLPClassifier(hidden_layer_sizes=(11,1000,2),solver='lbfgs',learning_rate_init=0.1,max_iter=50000, tol=0.1)
ml_cv = MLPClassifier(hidden_layer_sizes=(11,1000,2),solver='lbfgs',learning_rate_init=0.1,max_iter=50000, tol=0.1)

ml_10fcv.fit(formated_data["sys2"]["X_train"], formated_data["sys2"]["y_train"])
score_10fcv = ml_10fcv.score(formated_data["sys2"]["X_test"],formated_data["sys2"]["y_test"])
ml_cv.fit(formated_data["sys2"]["data"], formated_data["sys2"]["target"])
score_cv = ml_cv.score(formated_data["sys1"]["data"],formated_data["sys1"]["target"])

# First system prediction
pred_10fcv = ml_10fcv.predict(formated_data["sys1"]["data"])
accuracy_10fcv = accuracy_score(formated_data["sys1"]["target"],pred_10fcv)
pred_cv = ml_cv.predict(formated_data["sys1"]["data"])
accuracy_cv = accuracy_score(formated_data["sys1"]["target"],pred_cv)

# Stockage pour le formattage des données comparatives
formated_data["sys2"]["resultats"].append({'name':"Neural Network",'score_10fcv':score_10fcv,'score_cv':score_cv, 'accuracy_10fcv':accuracy_10fcv,'accuracy_cv':accuracy_cv})

# Confusion matrix
cm_10fcv = confusion_matrix(formated_data["sys1"]["target"], pred_10fcv)
cm_cv = confusion_matrix(formated_data["sys1"]["target"], pred_cv)

print_result(score_10fcv=score_10fcv, score_cv=score_cv, accuracy_10fcv=accuracy_10fcv, accuracy_cv=accuracy_cv, cm_10fcv=cm_10fcv, cm_cv=cm_cv)
